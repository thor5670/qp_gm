/*
 * ! 服务器管理
 */
Ext.onReady(function() {

	Ext.QuickTips.init();

	var serverlistObj = [{
				name : 'server_id',
				type : 'int'
			}, {
				name : 'server_name',
				type : 'string'
			}, {
				name : 'server_url',
				type : 'string'
			}];

	var store = new Ext.data.JsonStore({
				url : 'serverlist_findPageServer.do',
				root : 'root',
				totalProperty : 'total',
				autoLoad : {
					params : {
						start : 0,
						limit : 15
					}
				},
				fields : serverlistObj
			});

	var grid = new Ext.grid.GridPanel({
		store : store,
		columns : [new Ext.grid.RowNumberer(), {
					header : '服务器名称',
					width : 200,
					align : 'center',
					menuDisabled : true,
					dataIndex : 'server_name'
				}, {
					id : 'server_url',
					header : '服务器url',
					align : 'center',
					menuDisabled : true,
					dataIndex : 'server_url'
				}],
		stripeRows : true, // 行分隔符
		columnLines : true, // 列分隔符
		autoExpandColumn : 'server_url', // 自动扩展列
		loadMask : true, // 加载时的遮罩
		frame : true,
		title : '服务器管理',
		iconCls : 'menu-61',

		tbar : ['->', {
					text : '增加',
					iconCls : 'btn-add',
					handler : function() {
						serverWindow.show();
						serverAddForm.getForm().reset();
					}
				}, '-', {
					text : '修改',
					iconCls : 'btn-edit',
					handler : function() {
						var record = grid.getSelectionModel().getSelected();
						if (!record) {
							Ext.Msg.alert('信息提示', '请选择要修改的数据');
						} else {
							serverlistWindow.show();
							serverForm.getForm().loadRecord(record);
						}
					}
				}, '-', {
					text : '删除',
					iconCls : 'btn-remove',
					handler : function() {
						var record = grid.getSelectionModel().getSelected();
						if (!record) {
							Ext.Msg.alert('信息提示', '请选择要删除的数据');
						} else {
							Ext.MessageBox.confirm('删除提示', '是否删除该角色？',
									function(c) {
										if (c == 'yes') {
											Ext.Ajax.request({
												url : "serverlist_deleteServer.do",
												params : {
													server_id : record
															.get("server_id")
												},
												success : function(response) {
													var Result = Ext
															.decode(response.responseText);
													if (!Result.success) {
														Ext.Msg.alert("信息提示",
																Result.error);
														return;
													}
													store.reload();
												}
											});
										}
									});
						}
					}
				}],

		bbar : new Ext.PagingToolbar({
					pageSize : 15,
					store : store,
					displayInfo : true
				})
	});

	var serverAddForm = new Ext.FormPanel({
				layout : 'form',
				baseCls : 'x-plain',
				labelWidth : 60,
				border : false,
				padding : '20 10 0 8',
				defaults : {
					anchor : '100%',
					xtype : 'textfield'
				},
				items : [{
							name : 'server_name',
							fieldLabel : '服务器名称',
							maxLength : 20,
							allowBlank : false
						}, {
							xtype : 'textarea',
							name : 'server_url',
							fieldLabel : '服务器url',
							height : 80,
							maxLength : 100
						}, {
							xtype : 'hidden',
							name : 'server_id'
						}]
			});

	var serverWindow = new Ext.Window({
				title : '添加窗口',
				width : 400,
				height : 230,
				closeAction : 'hide',
				modal : true,
				layout : 'fit',
				buttonAlign : 'center',
				items : [serverAddForm],
				buttons : [{
					text : '保存',
					handler : function() {
						if (serverAddForm.getForm().isValid()) {
							serverAddForm.getForm().submit({
								url : 'serverlist_saveOrUpdateServer.do',
								success : function(form, action) {
									Ext.Msg
											.alert('信息提示',
													action.result.message);
									serverWindow.hide();
									store.reload();
								},
								failure : function(form, action) {
									if (action.result.errors) {
										Ext.Msg.alert('信息提示',
												action.result.errors);
									} else {
										Ext.Msg.alert('信息提示', '连接失败');
									}
								},
								waitTitle : '提交',
								waitMsg : '正在保存数据，稍后...'
							});
						}
					}
				}, {
					text : '取消',
					handler : function() {
						serverWindow.hide();
					}
				}]
			});

	new Ext.Viewport({
				layout : 'border',
				items : [{
							region : 'center',
							layout : 'fit',
							border : false,
							items : grid
						}]
			});

});
