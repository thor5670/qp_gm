package myInterceptorStruts;

import org.springframework.web.struts.ActionSupport;

public class WrongTest extends ActionSupport
{
    public String execute() throws Exception
    {
    	System.out.println("----------Exception");
        throw new Exception("hello exception") ;
    }
}
