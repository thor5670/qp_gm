package com.cxstock.biz.serverlist.imp;

import java.util.ArrayList;
import java.util.List;

import com.cxstock.biz.serverlist.ServerlistBiz;
import com.cxstock.biz.serverlist.dto.ServerlistDTO;
import com.cxstock.dao.BaseDAO;
import com.cxstock.pojo.Role;
import com.cxstock.pojo.Serverlist;
import com.cxstock.utils.pubutil.ComboData;
import com.cxstock.utils.pubutil.Page;
@SuppressWarnings("unchecked")
public class ServerlistBizImpl implements ServerlistBiz {

	private BaseDAO baseDao;

	public void setBaseDao(BaseDAO baseDao) {
		this.baseDao = baseDao;
	}

	/*
	 * 分页查询服务器列表
	 */
	public void findPageServer(Page page) {
		// TODO Auto-generated method stub
		String hql = "from Serverlist order by server_id";
		List list = baseDao.findByHql(hql, page.getStart(), page.getLimit());
		List dtoList = ServerlistDTO.createDtos(list);
		int total = baseDao.countAll("Serverlist");
		page.setRoot(dtoList);
		page.setTotal(total);

	}

	/*
	 * 保存/修改服務器
	 */
	public void saveOrUpdateServer(ServerlistDTO dto) {
		
		Serverlist server = new Serverlist();
		if (dto.getServer_id() != null) {
			server = (Serverlist) baseDao.loadById(Serverlist.class,
					dto.getServer_id());
		}
		server.setServer_name(dto.getServer_name());
		server.setServer_url(dto.getServer_url());
		System.out.println("---ServerlistBizImpl-saveOrUpdateServer---server="+server);
		baseDao.saveOrUpdate(server);
	}

	/*
	 * 刪除服務器
	 */
	public boolean deleteServer(Integer server_id) {
		
		String hql = "select count(server_id) from Serverlist where server_id = "
				+ server_id;
		int count = baseDao.countQuery(hql);
		System.out.println("----------count="+count);
		if (count <= 0) {
			return false;
		} else {
			baseDao.deleteById(Serverlist.class, server_id);
			return true;
		}
	}

	/*
	 * 服务器下拉数据
	 */
	public List findServerType() {
		List list = new ArrayList();
		List<Serverlist> serverList = baseDao.listAll("Serverlist");
		for (Serverlist server : serverList) {
			ComboData dto = new ComboData();
			dto.setValue(server.getServer_id().toString());
			dto.setText(server.getServer_name());
			list.add(dto);
		}
		return list;
	}

}
