package com.cxstock.biz.serverlist;

import java.util.List;

import com.cxstock.biz.serverlist.dto.ServerlistDTO;
import com.cxstock.utils.pubutil.Page;


public interface ServerlistBiz {
	/**
	 * 分页查询用户列表
	 */
	public void findPageServer(Page page);
	
	/**
	 * 保存/修改 服务器
	 */
	public void saveOrUpdateServer(ServerlistDTO dto);
	
	
	/**
	 * 删除服务器
	 */
	public boolean deleteServer(Integer server_id);
	
	/**
	 * 角色下拉数据
	 */
	
	public List findServerType();
	
	


}
