package com.cxstock.biz.serverlist.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.cxstock.pojo.Serverlist;

public class ServerlistDTO {
	private Integer server_id;
	private String server_name;
	private String server_url;

	public ServerlistDTO() {
		super();
	}

	public ServerlistDTO(Integer server_id, String server_name, String server_url) {
		super();
		this.server_id = server_id;
		this.server_name = server_name;
		this.server_url = server_url;
	}

	public static ServerlistDTO createDto(Serverlist pojo) {
		ServerlistDTO dto = null;
		if (pojo != null) {
			dto = new ServerlistDTO(pojo.getServer_id(), pojo.getServer_name(),
					pojo.getServer_url());
		}
		return dto;
	}

	public static List createDtos(Collection pojos) {
		List<ServerlistDTO> list = new ArrayList<ServerlistDTO>();
		if (pojos != null) {
			Iterator iterator = pojos.iterator();
			while (iterator.hasNext()) {
				list.add(createDto((Serverlist) iterator.next()));
			}
		}
		return list;
	}

	public Integer getServer_id() {
		return server_id;
	}

	public void setServer_id(Integer server_id) {
		this.server_id = server_id;
	}

	public String getServer_name() {
		return server_name;
	}

	public void setServer_name(String server_name) {
		this.server_name = server_name;
	}

	public String getServer_url() {
		return server_url;
	}

	public void setServer_url(String server_url) {
		this.server_url = server_url;
	}

}
