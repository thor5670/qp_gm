package com.cxstock.action.serverlist;

import com.cxstock.action.BaseAction;
import com.cxstock.biz.serverlist.ServerlistBiz;
import com.cxstock.biz.serverlist.dto.ServerlistDTO;
import com.cxstock.utils.pubutil.Page;

public class ServerlistAction extends BaseAction {

	private ServerlistBiz serverlistBiz;

	private Integer server_id;
	private String server_name;
	private String server_url;

	/**
	 * 分页查询服务器
	 */
	public String findPageServer() {
		System.out.println("--------findPageServer---");
		try {
			Page page = new Page();
			page.setStart(this.getStart());
			page.setLimit(this.getLimit());
			serverlistBiz.findPageServer(page);
			this.outPageString(page);
		} catch (Exception e) {
			e.printStackTrace();
			this.outError();
		}
		return null;
	}

	/**
	 * 保存修改服务器
	 */
	public String saveOrUpdateServer() {
		System.out.println("-----------saveOrupdateServer---server_id="
				+ server_id);
		try {
			ServerlistDTO dto = new ServerlistDTO(server_id, server_name,
					server_url);
			serverlistBiz.saveOrUpdateServer(dto);
			if (server_id == null) {
				this.outString("{success:true,message:'保存成功!'}");
			} else {
				this.outString("{success:true,message:'修改成功!'}");
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.outError();
			System.out.println("-----Exception------ e=" + e);
		}
		return null;
	}

	/**
	 * 删除服务器
	 */
	public String deleteServer() {
		System.out.println("----deleteServer-------server_id=" + server_id);
		try {
			boolean b = serverlistBiz.deleteServer(server_id);
			System.out.println("-------b=" + b);
			if (b) {
				this.outString("{success:true}");
			} else {
				this.outString("{success:false,error:'该服务器已被使用，不能删除'}");
			}
		} catch (Exception e) {
			System.out.println("----Exception-------e=" + e);
			// TODO: handle exception
		}
		return null;

	}

	/**
	 * 服务器下拉数据
	 */
	public String findRoleType() {
		System.out.println("-------findRoleType----");
		try {
			this.outListString(serverlistBiz.findServerType());
		} catch (Exception e) {
			e.printStackTrace();
			this.outError();
		}
		return null;
	}

	public ServerlistBiz getServerlistBiz() {
		return serverlistBiz;
	}

	public void setServerlistBiz(ServerlistBiz serverlistBiz) {
		this.serverlistBiz = serverlistBiz;
	}

	public Integer getServer_id() {
		return server_id;
	}

	public void setServer_id(Integer server_id) {
		this.server_id = server_id;
	}

	public String getServer_name() {
		return server_name;
	}

	public void setServer_name(String server_name) {
		this.server_name = server_name;
	}

	public String getServer_url() {
		return server_url;
	}

	public void setServer_url(String server_url) {
		this.server_url = server_url;
	}
}
