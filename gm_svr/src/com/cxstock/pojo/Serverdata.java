package com.cxstock.pojo;

import com.cxstock.utils.system.DateTime;

public class Serverdata implements java.io.Serializable {
	private Integer dataid;
	private Integer uid;
	private Integer sort;
	private DateTime time;
	private Integer num;

	public Serverdata() {
	};

	public Serverdata(Integer uid) {
		this.uid = uid;
	};
	
	public Serverdata(DateTime time) {
		this.time = time;
	};
	
	public Serverdata(Integer dataid,Integer uid,Integer sort,DateTime time,Integer num) {
		this.dataid = dataid;
		this.uid=uid;
		this.sort = sort;
		this.time = time;
		this.num=num;
	}

	public Integer getDataid() {
		return dataid;
	}

	public void setDataid(Integer dataid) {
		this.dataid = dataid;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public DateTime getTime() {
		return time;
	}

	public void setTime(DateTime time) {
		this.time = time;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	};
	
	


}
