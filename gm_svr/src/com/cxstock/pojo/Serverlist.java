package com.cxstock.pojo;

import java.util.Date;

public class Serverlist implements java.io.Serializable {
	private Integer server_id;
	private String server_url;
	private String server_name;
	/** default constructor */
	public Serverlist() {
	}
	
	public Serverlist(Integer server_id) {
		this.server_id = server_id;
	}

	/** minimal constructor */
	public Serverlist(String server_name) {
		this.server_name = server_name;
	}
	/** full constructor */
	public Serverlist(String server_name, String server_url) {
		this.server_name = server_name;
		this.server_url = server_url;
	}
	public Serverlist(Integer server_id, String server_name, String server_url) {
		this.server_id = server_id;
		this.server_name = server_name;
		this.server_url = server_url;
	}

	public Integer getServer_id() {
		return server_id;
	}

	public void setServer_id(Integer server_id) {
		this.server_id = server_id;
	}

	public String getServer_url() {
		return server_url;
	}

	public void setServer_url(String server_url) {
		this.server_url = server_url;
	}

	public String getServer_name() {
		return server_name;
	}

	public void setServer_name(String server_name) {
		this.server_name = server_name;
	}
	
}
