package com.cxstock.utils.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cxstock.biz.power.dto.UserDTO;
import com.cxstock.utils.system.Constants;


public class SecurityFilter implements Filter {
	@SuppressWarnings("unused")
	private FilterConfig filterCon = null;
	
	public void init(FilterConfig config) throws ServletException {
		System.out.println("------SecurityFilter---------config="+config);
		filterCon = config;
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain)  {
		System.out.println("---SecurityFilter-----doFilter--------");
		try {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			UserDTO userInfo = (UserDTO) httpRequest.getSession().getAttribute(Constants.USERINFO);
			String str=httpRequest.getRequestURL().toString();
			System.out.println("-----SecurityFilter------str="+str+",userInfo="+userInfo);
			if(userInfo==null){
				if(str.indexOf("/login.jsp")==-1){
					httpResponse.sendRedirect(httpRequest.getContextPath()+"/login.jsp");
				}else{
					filterChain .doFilter(request, response);
				}
			}else{
				filterChain .doFilter(request, response);		
			}
			
		} catch (Exception e) {
			System.out.println("----------------e="+e);
			// TODO: handle exception
		}
		
		
	}

	public void destroy() {
		filterCon = null;
	}
}
